<?php 
 require ('conexion.php');

//  El tipo de conexion es PDO buscar para poder entender mejor.


 class Usuario //clase usuario
 {
 
    public function Insertar_Estudiante($data){
        //es la data del formulario, que viene del archivo register_login.php que se encuentra en la carpeta controllers.
        $cedula = $data['cedula'];
        $foto = $data['foto'];
        $nombre = $data['nombre'];
        $correo = $data['correo'];
        $telefono = $data['telefono'];
        $contrasena = $data['contrasena']; 
        //se esta separando por variable para asi poder guardarlos de manera separada.

        $conn = Conexion::conectar();//abiendo la conexion.

        $statement = $conn->prepare("INSERT INTO usuarios (cedula, foto, nombre,correo,telefono,contrasena)  VALUES (?, ?, ?, ?, ?, ?)");//se prepara el insert
        // true o false

        $exito = $statement->execute([$cedula, $foto, $nombre,$correo,$telefono,$contrasena]); 
        //se ejecuta el insert con los parametros que se colocaron en signos de ??..
      //en la variable exito se guarda un true o un false retornado por la linea 26.
      if ($exito) {//verifica si se inserto para luego mandar un exito ok o un error.false
      
            return "ok";
        }else{
            return "error";
        } 

        $statement=null;
    }

    public function verificar_cedula($cedula){ 
      $conn = Conexion::conectar();
      
      $sql = "SELECT * FROM  usuarios as a  WHERE cedula = '$cedula'";
      $statement = Conexion::conectar()->prepare($sql);
      $statement->execute(); 
        //para que sive el fetchall https://www.php.net/manual/es/pdostatement.fetchall.php
      return $statement->fetchAll();
    }

    public function Ingreso_Sistema($data)
    { 
        $email = $data['email'];
        $pass = $data['pass'];
    
        $conn = Conexion::conectar();
        
        $sql = "SELECT * FROM  usuarios as a  WHERE correo = '$email' &&  contrasena = '$pass' && activo=1";
        
        $statement = Conexion::conectar()->prepare($sql);
        $statement->execute(); 
      // var_dump($statement->fetch());
        //para que sive el fetch https://www.php.net/manual/es/pdostatement.fetchall.php
      //fetch para traer solo un resultado.
      //fetchAll para traer una collecion
      return $statement->fetch();
    }

    public function Insertar_administrativos(){ 

      $encriptar = crypt('123456789', '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
      $2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');
      $cedula ='9-444-222';
      $foto = '../imagenes/default.png';
      $nombre = 'Vicedecano-3';
      $correo = 'vicedecano3@utp.ac.pa';
      $telefono = '6565-8969';
      $contrasena =$encriptar; 

      $conn = Conexion::conectar();
      $statement = $conn->prepare("INSERT INTO usuarios (cedula, foto, nombre,correo,telefono,contrasena,tipo)  VALUES (?, ?, ?, ?, ?, ?, ?)");//se prepara el insert
      $exito = $statement->execute([$cedula, $foto, $nombre,$correo,$telefono,$contrasena,'2']); //se ejecuta el insert con los parametros que se colocaron en signos de ??..
      //en la variable exito se guarda un true o un false retornado por la linea 19.
      if ($exito) {//verifica si se inserto para luego mandar un exito ok o un error.
          return "ok";
      }else{
          return "error";
      } 
      $statement=null;
    }

    //insertar el key para la verficacion del usuario.
    public function insert_key($key,$cedula){
      
      $key_email =$key;

      $conn = Conexion::conectar();
      $statement = $conn->prepare("UPDATE  usuarios SET  key_email=?  WHERE cedula ='$cedula'");
      $exito = $statement->execute([$key_email]); 
     
      if ($exito) {
          return "ok";
      }else{
          return "error";
      } 
      $statement=null; //cerrar la conexion.
    }
    //validamos un key para poder activar cuenta.
    public function verificar_key($key){ 
      $conn = Conexion::conectar();
      
      $sql = "SELECT * FROM  usuarios WHERE key_email = '$key'";
      $statement = Conexion::conectar()->prepare($sql);
      $statement->execute(); 

      //para que sive el fetchall https://www.php.net/manual/es/pdostatement.fetchall.php

      return $statement->fetchAll();
    }
    //EXPLCIAR AHORA.
    public function update_key($key){
      $key_email ="";
      $activo =1;
      $conn = Conexion::conectar();
      $statement = $conn->prepare("UPDATE  usuarios SET  key_email=?,activo=?  WHERE key_email ='$key'");
      $exito = $statement->execute([$key_email,$activo]);
      if ($exito) {
          return "ok";
      }else{
          return "error";
      } 
      $statement=null;
    }

 }
 ?>