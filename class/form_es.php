<?php
require('conexion.php');

class Form
{
    public function formulario($data)
    {

        $cedula = $data['cedula'];
        $sede = $data['sede'];
        $carrera = $data['carrera'];
        $titulo = $data['txt_titulo'];
        $objetivo = $data['txt_objetivo'];
        $arc = $data['arc'];
        $pc = $data['pc'];
        $ids = $data['ids'];
        $css = $data['css'];
        $siceri = $data['siceri'];
        $titulo_linc = $data['titulo_linc'];
        $asesor = $data['asesor'];
        $depa_as = $data['depa_as'];
        $empresa = $data['empresa'];
        $nombre2 = $data['nombre2'];
        $cedula2 = $data['cedula2'];
        $telefono2 = $data['telefono2'];
        $correo2 = $data['correo2'];
        $othe_asesor = $data['othr_asesor'];
        $estado = $data['estado'];
        $radio= $data['rdbtipo'];
        $check= $data['check1'];

        if (isset($radio)) {
            $tipo = $radio;
        } elseif (isset($check)) {
            $tipo =$check;
        } elseif (isset($check)) {
            $tipo = $data['txt_otro'];
        }

        $conn = Conexion::conectar();

        $sql = "INSERT INTO proyectos (cedula,sede,carrera,titulo,objetivo,arc,pc,ids,css,siceri,tipo,asesor,depa_as,titulo_linc,empresa,nombre2,cedula2,telefono2,correo2,othe_asesor,estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $statement = $conn->prepare($sql);

        $exito = $statement->execute([$cedula, $sede, $carrera, $titulo, $objetivo, $arc ,$pc, $ids, $css, $siceri,$tipo, $asesor, $depa_as, $titulo_linc, $empresa, $nombre2,$cedula2, $telefono2, $correo2, $othe_asesor, $estado]);

        if ($exito) {
            return "ok";
        } else {
            return "error";
        }
        $statement = null;
    }
    
}
