<?php 
// ob_start();

session_start();

require '../class/usuario.php'; //la clase
require '../controllers/send_mail.php';// el controlador de correo.

$obj = new Usuario();
$email = new sendEmail();

/*Roles
*   
    Estudiante =1;
    Secretaria = 2;
    vicedecano = 3;

*/

if(isset($_POST["registrar"])){

$exite =$obj->verificar_cedula($_POST["cedula"]);
 
if(count($exite)== 0){
    if ($_POST["password"] == $_POST["password_2"]) {
    
        if (isset($_POST["email"]) && isset($_POST["cedula"]) && isset($_POST["telefono"]) && isset($_POST["nombre"])  ) {
          
                      $encriptar = crypt($_POST["password"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
                              $2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');
                      // arreglo llave-valor
                      $data = array(
                          'cedula'=> $_POST["cedula"],
                          'foto'=> '../imagenes/default.png',
                          'nombre'=> $_POST["nombre"],
                          'correo'=> $_POST["email"],
                          'telefono'=> $_POST["telefono"], 
                          'contrasena'=> $encriptar 
                        );
                
                        
        
                   $resultado = $obj->Insertar_Estudiante($data);
        
                     if ($resultado == "ok"){ 
                   $_SESSION["error"] = "ok2";
                   $_SESSION["mensaje2"]= "REGISTRADO CORRECTAMENTE, SE LE ENVIO UN EMAIL PARA LA VERIFICACIÓN";

                  /******************************************************************************************************* */
                   $key = $encriptar = crypt($_POST["cedula"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
                   $2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');
                   $dominio_actual = $_SERVER['HTTP_HOST'];//optiene el dominio actual, para que sea de manera dinamico.
                 
                   $data =[
                     "para"=>$_POST["email"],
                     "name"=>$_POST["nombre"],
                     "sujeto"=>"Prueba para semestral",
                     "body"=>"<div style='padding: 8%;background:#343a40!important ;color: white; text-align: justify'><h5 style='font-family:Arial, Helvetica, sans-serif;font-size:20px'>Bienvenido, ".$_POST["nombre"]." al sistema de propuestas de proyectos finales, para poder empezar el proceso, debe confirmar el correo para que su cuenta se active automaticamente.</h5><a style='padding: 2%;background:blue !important;color: white;font-size:20px;font-weight: bold;border-radius:10%; text-decoration: none' href='http://".$dominio_actual."/proyecto-final/login/confirmacion.php?key=" . $key . "'>Confirmar Cuenta</a></div>"
                   ];
                    $email->EnviarCorreo($data);//envia el correo.
                    $obj->insert_key($key,$_POST["cedula"]); //inserta hash o key para la verificacion del correo.

                     /******************************************************************************************************* */
                     
                   header('Location: ../login/Registrar.php');
            
                     } 
        
        }else{
            $_SESSION["error"] = "ok";
            $_SESSION["mensaje"]= "LLENE TODOS LOS CAMPOS";
            // header('Location: ../login/Registrar.php');
        }
    }else{
        
        $_SESSION["error"] = "ok";
        $_SESSION["mensaje"]= "LAS CONTRASEÑAS NO COINCIDEN";
         header('Location: ../login/Registrar.php');
    }

}else{
    $_SESSION["error"] = "ok";

    $_SESSION["mensaje"]= "La Cedula ya existe"; 
 
     header('Location: ../login/Registrar.php');
}
 
   }elseif (isset($_POST["login"])) {
     
  
    // $obj->Insertar_administrativos(); //metodo para agregar usuarios administrativos, secretaria o vicedecano.
    if (isset($_POST["email"]) && isset($_POST["password"])){
        
      $encriptar = crypt($_POST["password"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$
      $2a$07$asxx54ahjppf45sd87a5auxq/SS293XhTEeizKWMnfhnpfay0AALe');

    
      $data = array('email' => $_POST["email"],'pass' => $encriptar );
       
       $respuesta = $obj->Ingreso_Sistema($data);
   
       $temp = $respuesta['tipo'];
     if ($respuesta != false) { 
           $cantidad= count($respuesta);
      
              if ($cantidad >1) {
       
               //borrando una session en especifico
                   unset($_SESSION["errorS"]);
                   unset($_SESSION["error"]);
                   unset($_SESSION["mensaje"]);
                   unset($_SESSION["mensaje2"]);
               //creando session
              
               $_SESSION["session"]="ok";
                   $_SESSION["tem"]=$respuesta["tipo"];
                   $_SESSION["cedula"] =$respuesta["cedula"];
                   $_SESSION["foto"] =$respuesta["foto"];
                   $_SESSION["nombre"] =$respuesta["nombre"];
                   $_SESSION["correo"] =$respuesta["correo"];
                   $_SESSION["telefono"] =$respuesta["telefono"];
                   
                   //SESSIONES LISTA PARA SU USO.
                    //  header("Location: ../Vicedecano_Academico/home.php");
                    error_reporting(E_ALL | E_WARNING | E_NOTICE);
                    ini_set('display_errors', TRUE);
                    include '../config/temp.php';
                    // header("Location: ../Vicedecano_Academico/home.php");
                    // die('should have redirected by now');
                    
              }else{
                unset($_SESSION["error"]);
                unset($_SESSION["mensaje"]);
                unset($_SESSION["mensaje2"]);
                $_SESSION["errorS"]="ERROR";
                header('Location: ../login/login.php');
              }

     }else{
        $_SESSION["error"] = "ok";
        $_SESSION["mensaje"]= "LAS CREDENCIALES SON INCORRECTAS";
         header('Location: ../login/login.php');
     }
      
    
    
    
    
    }else{
      $_SESSION["error"] = "ok";
      $_SESSION["mensaje"]= "LLENE TODOS LOS CAMPOS";
       header('Location: ../login/login.php');
     }
    
    
     
    } 

?>