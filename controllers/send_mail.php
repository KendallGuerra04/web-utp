<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../PHPMailer/src/Exception.php';
require '../PHPMailer/src/PHPMailer.php';
require '../PHPMailer/src/SMTP.php';
require '../config/variables.php'; //archivo donde estan las variables globales y estan static por eso se llama con :: puntos.
class sendEmail {

    public function EnviarCorreo($data)
    {
        // echo "hola"; 
        $mail = new PHPMailer(true);
        
        //En el archivo config/variables.php estan las variables de manera static globales las cuales son usadas para el username y password para el envio de correo.
        
        try {
            //Server settings
            $mail->SMTPDebug = 2;                                       // Habilitar la salida de depuración detallada
            $mail->isSMTP();                                            // Enviar usando SMTP
            $mail->Host       = 'smtp.gmail.com';                    // Configure el servidor SMTP para enviar
            $mail->SMTPAuth   = true;                                   // Habilitar la autenticación SMTP
            $mail->Username   = variablesEntorno::$email;                     // SMTP correo
            $mail->Password   = variablesEntorno::$password;                                  // SMTP contraseña
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Habilite el cifrado TLS; Se recomienda `PHPMailer :: ENCRYPTION_SMTPS`
            $mail->Port       = 587;                                    // Puerto TCP para conectarse, use 465 para `PHPMailer :: ENCRYPTION_SMTPS` arriba
        
            //Recipients
            $mail->setFrom(variablesEntorno::$email, variablesEntorno::$name);
            $mail->addAddress($data['para'], $data['name']);     // Aa quien se le va enviar
        
            // Archivos adjuntos
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Archivos adjuntos
            // $mail->addAttachment('/tmp/image.j pg', 'new.jpg');    // Archivos adjuntos nombre diferente
        
            // Contenido
            $mail->isHTML(true);                                  // Establecer el formato de correo electrónico en HTML
            $mail->Subject = $data['sujeto'];
            $mail->Body    = $data['body'];
        
            $mail->send();
            // echo 'Mensaje Enviado';
        } catch (Exception $e) {
            // echo "Error: {$mail->ErrorInfo}";
        }
    }

}