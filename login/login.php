<?php 
session_start();
session_destroy();
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/login.css" media="screen" />

    <title>Iniciar_sesión</title>
  </head>
  <body>
    <!-----------Barra de Navegación ----->
    <!--Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../login/login.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="https://utp.ac.pa/">Portal UTP
                <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="https://correo.utp.ac.pa/">Correo UTP
                    <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="https://matricula.utp.ac.pa/">Matricula UTP
                        <span class="sr-only">(current)</span>
                        </a>
                      </li>
                          <li class="nav-item ">
                            <a class="navbar-brand" href="https://utp.ac.pa/antecedentes-historicos-de-la-universidad-tecnologica-de-panama">Conocenos
                            <span class="sr-only">(current)</span>
                            </a>
                      </li>
            </ul>
          </div> 
    </div>
  </nav>
  <!--/.Navbar -->

<!--------Contenido ------->
<div class="login-form">
    <form action="../controllers/registrar_login.php" method="POST">
    <?php 
                   if (isset($_SESSION["errorS"])) {
                   	  if ($_SESSION["errorS"] == "ERROR") {
                   	  ?>
                   	  <h6 class="alert alert-danger">Contraseña o correo Incorrectos</h6>
                   	  <?php
                   	  } 
                   	}
		 		 ?>
        <h2 class="text-center">Iniciar Sesión</h2>       
        <div class="form-group">
            <label>Correo :</label>
            <input type="email" class="form-control" name="email" placeholder="Correo Electronico" >
        </div>
        <div class="form-group">
            <label>Contraseña :</label>
            <input type="password" class="form-control" name="password" placeholder="..............." >
        </div> 
        <div class="form-group clearfix text-center">
            <button type="submit" class="btn btn-primary btn-lg" name="login">Iniciar Sesión</button>
            </div>
        <div class="text-center">
          <a href="../login/Registrar.php" class="btn btn-danger">No tienes cuenta?</a>
        </div>
    </form>
<br>
  <div class="text-center">
  <?php 
                   if (isset($_SESSION["error"])) {
                   	  if ($_SESSION["error"] =="ok2") {
                   	  ?>
                   	  <h6 class="alert alert-success"><?php echo $_SESSION["mensaje2"] ?></h6>
                   	  <?php
                   	  }elseif($_SESSION["error"] =="ok"){
                       ?>
                        <h6 class="alert alert-danger"><?php echo $_SESSION["mensaje"] ?></h6>
                       <?php
                   	  }
                   }
          ?>
  </div>
          
</div>
<!------/.Contenido ---->
    <!-- Script-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>