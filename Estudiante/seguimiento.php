<?php 
session_start();
require '../class/conexion.php'; 
?>
<?php 
   if (isset($_SESSION["cedula"])) {
     $cedula = $_SESSION["cedula"];
     if ($_SESSION["tem"] == "1") {
   	 	
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/med_form.css" media="screen" />

    <title>Seguimiento</title>
  </head>
  <body>
<!--Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../Estudiante/home.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Estudiante/home.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="../Estudiante/Trab_grad.php">Registro Trabajo de Graduación
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="../Estudiante/Repositorio.php">Repositorio
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                          <li class="nav-item ">
                            <a class="navbar-brand" href="../Estudiante/seguimiento.php">Seguimiento
                              <span class="sr-only">(current)</span>
                            </a>
                      </li>
            </ul>
              <ul class="navbar-nav navbar-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                      <img src="<?php  
                        echo $_SESSION["foto"]; 
                                ?>
                       " class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                                <?php  
                                      echo $_SESSION["nombre"]; 
                                  ?>
                  </a>
                      <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                        aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                      </div>
                </li>
              </ul>
          </div> 
    </div>
  </nav>
<!--/.Navbar -->
<?php
  $sql = "CALL Progreso('$cedula')";
  $statement = Conexion::conectar()->prepare($sql);
  $statement->execute(); 
  $results = $statement->fetch(PDO::FETCH_ASSOC);
  $result = $results;
?>

<div class="abs-center">
    <div class="border p-5 form border border-dark shadow-lg p-4 mb-4 bg-white" >
        <h2 class="text-center">Universidad Tecnológica de Panamá</h2>
        <h6 class="text-center">Anteproyectos</h6>
        <p>Progreso :</p>
        <div class="progress">
          <div class="progress-bar" role="progressbar" style="width: <?php echo $result['total'] . '%'?>;" aria-valuenow="<?php echo $result['total']; ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $result['total'] . ' % ' ?></div>
        </div>
        <div class="form-group">
        <button id="control" onclick="location.href='../Estudiante/seg_estu.php'" type="button" class="btn btn-info">Mostrar</button>
    </div>
  </div>
</div>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>

<?php }else{
         header('Location: ../login/login.php');

}
 }else{
         header('Location: ../login/login.php');

} ?>