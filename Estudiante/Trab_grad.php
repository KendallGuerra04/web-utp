<?php
session_start();
require ('../class/conexion.php');
?>
<?php
if (isset($_SESSION["cedula"])) {
  if ($_SESSION["tem"] == "1") {

?>
    <!doctype html>
    <html lang="en">

    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="../css/form.css" media="screen" />

      <title>Estudiante/Trabajo-de-Graduación</title>
    </head>

    <body style="background-image: url(../imagenes/WebBackground.jpg); background-repeat: repeat; background-size: contain">
      <!------Barra de Navegación ------->
      <!--Navbar -->
      <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="../Estudiante/home.php">
            <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            GRADUACIÓN FISC
          </a>
          <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Estudiante/home.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="navbar-brand" href="../Estudiante/Trab_grad.php">Registro Trabajo de Graduación
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="navbar-brand" href="../Estudiante/Repositorio.php">Repositorio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item ">
                <a class="navbar-brand" href="../Estudiante/seguimiento.php">Seguimiento
                  <span class="sr-only">(current)</span>
                </a>
              </li>
            </ul>
            <ul class="navbar-nav navbar-right nav-flex-icons">
              <li class="nav-item avatar dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="<?php
                            echo $_SESSION["foto"];
                            ?>
                       " class="rounded-circle z-depth-0" alt="avatar image" height="35">
                  <?php
                  echo $_SESSION["nombre"];
                  ?>
                </a>
                <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary" aria-labelledby="navbarDropdownMenuLink-55">
                  <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!--/.Navbar -->

      <!----Content --->
      <div class="abs-center">
        <form action="../controllers/registrar_form.php" class="border p-5 form border border-dark shadow-lg p-4 mb-4 bg-white" method="POST" enctype="multipart/form-data">
        <?php
        if (isset($_SESSION["error"])) {
          if ($_SESSION["error"] == "Success") {
        ?>
            <h6 class="alert alert-success text-center" ><?php echo $_SESSION["mensaje1"] ?></h6>
          <?php
          } elseif ($_SESSION["error"] == "Error") {
          ?>
            <h6 class="alert alert-danger text-center"><?php echo $_SESSION["mensaje2"] ?></h6>
        <?php
          }
        }
        ?>
          <p class="text-center">Universidad Tecnológica de Panamá <br>
            Facultad de Ingenieria de Sistemas Computacionales <br>
            Registro Oficial del tema de trabajo de Graduación
          </p>
          <div class="form-group">
            <label for="sede">Sede :</label>
            <select class="form-control" id="sede" name="sede">
              <option selected disabled>Seleccione la sede :</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'SL'";
              $result=$conn->query($sql);
              while($sede=$result->fetchColumn()){
              ?>
              <option value="<?php echo $sede; ?>"><?php echo $sede; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>

          <div class="form-group">
          <label for="carrera">Carrera :</label>
            <select class="form-control" id="Regionales" name="carrera">
              <option selected disabled value="">Seleccione la Carrera :</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'SLR'";
              $result=$conn->query($sql);
              while($sede=$result->fetchColumn()){
              ?>
              <option value="<?php echo $sede; ?>"><?php echo $sede; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>
          <p>Profesor(a)<br>
            VICEDECANO(A) ACADÉMICO(A)<br>
            Facultad de Ingenieria de Sistemas Computacionales<br> <br>
            Estimado(a) Vicedecano(a):<br>
          </p>
          <div class="form-group">
            <label for="txtbox">Por este medio le informamos que el Título del Trabajo de Graduación que hemos escogido es :</label><br>
            <textarea class="form-control" id="txtbox" rows="3" name="txt_titulo"></textarea>
          </div>
          <div class="form-group">
            <label for="txtbox">El mismo lleva como Objetivo General</label><br>
            <textarea class="form-control" id="txtbox" rows="3" name="txt_objetivo"></textarea>
          </div>
          <p>El tema Escogido tiene mayor relevancia en el área académica de:(Ponderar en rangos de 5 a 1, donde 5 es<br>
            para el departamento de mayor afinidad y 1 para el departamento de menor afinidad)</p>

          <div class="form-group">
            <label for="rango">Arquitectura y Redes de Computacionales</label>
            <select class="form-control" id="rango" name="arc">
              <option selected disabled>Seleccione el rango</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'arc'";
              $result=$conn->query($sql);
              while($arc=$result->fetchColumn()){
              ?>
              <option value="<?php echo $arc; ?>"><?php echo $arc; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>
          <div class="form-group">
            <label for="rango">Programación de Computadoras</label>
            <select class="form-control" id="rango" name="pc">
              <option selected disabled value="">Seleccione el rango</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'pc'";
              $result=$conn->query($sql);
              while($pc=$result->fetchColumn()){
              ?>
              <option value="<?php echo $pc; ?>"><?php echo $pc; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>
          <div class="form-group">
            <label for="rango">Ingenieria de Software</label>
            <select class="form-control" id="rango" name="ids">
              <option selected disabled value="">Seleccione el rango</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'ids'";
              $result=$conn->query($sql);
              while($ids=$result->fetchColumn()){
              ?>
              <option value="<?php echo $ids; ?>"><?php echo $ids; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>
          <div class="form-group">
            <label for="rango">Computación y Simulación de Sistemas</label>
            <select class="form-control" id="rango" name="css">
              <option selected disabled value="">Seleccione el rango</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'css'";
              $result=$conn->query($sql);
              while($css=$result->fetchColumn()){
              ?>
              <option value="<?php echo $css; ?>"><?php echo $css; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>
          <div class="form-group">
            <label for="rango">Sistemas de Información, Control y Evaluación de Recursos Informáticos</label>
            <select class="form-control" id="rango" name="siceri">
              <option selected disabled value="">Seleccione el rango</option>
              <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'siceri'";
              $result=$conn->query($sql);
              while($siceri=$result->fetchColumn()){
              ?>
              <option value="<?php echo $siceri; ?>"><?php echo $siceri; ?></option>
              <?php
              } 
             ?>
            </select>
          </div>
          <p>Este trabajo de Graduación lo consideramos de tipo:</p>
          
          <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'rad'";
              $result=$conn->query($sql);
              while($rad=$result->fetchColumn()){
              ?>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="rdbtipo" id="rdbteorico" value="<?php echo $rad; ?>">
            <label class="form-check-label" for="rdbteorico">
            <?php echo $rad; ?>
            </label>
            </div> 
          <?php
              } 
             ?>

            <?php 
              $conn = Conexion::conectar();
              $sql = "SELECT formato from formulario where tipo = 'check'";
              $result=$conn->query($sql);
              while($check=$result->fetchColumn()){
              ?>
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="check1" id="prac" value="<?php echo $check; ?>" onchange="javascript:showContent1()">
            <label class="form-check-label" for="prac">
            <?php echo $check; ?>
            </label>
          </div>
          <?php
              } 
             ?>

          <!-----Codigo que se libera por el Java-script ----->
          <div id="content1" style="display: none;">
            <div class="form-group">
              <label>Práctica Profesional, Nombre de la Empresa:</label><br>
              <input class="form-control" type="text" name="txt_otro_pracprof">
            </div>
          </div>
          <!---./Finaliza----->
          <div class="form-check">
            <input class="form-check-input" type="checkbox" name="check1" value="" id="check" onchange="javascript:showContent()">
            <label class="form-check-label">
              Otro
            </label>
          </div>
          <!-----Codigo que se libera por el Java-script ----->
          <div id="content2" style="display: none;">
            <div class="form-group">
              <label for="txtotro">Si es Otro, especifique:</label>
              <input class="form-control" type="text" name="txt_otro" id="txtotro">
            </div>
          </div>
          <!---./Finaliza----->
          <div class="form-group">
            <label for="txttitulo_lic">Para Optar por el Titulo de Licenciatura en:</label>
            <input class="form-control" type="text" name="titulo_lic" id="txttitulo_lic" >
          </div>
          <div class="form-group">
            <label for="txtasesor">Sugerimos como Asesor al Profesor(a):</label>
            <input class="form-control" type="text" name="asesor" id="txtasesor" >
          </div>
          <div class="form-group">
            <label for="txtacademico">El cual pertenece al Departamento Académico:</label>
            <input class="form-control" type="text" name="departamento" id="txtacademicoq" >
          </div>

          <div class="form-group">
            <button class="btn btn-primary" onclick="return mostrarOcultar('ocul')" type="button">Agregar Otro Estudiante</button>
            <div id="ocul" style="display: none;">
              <div class="form-group w-50 p-3">
                <label for="txtnombre">Nombre :</label>
                <input class="form-control" type="text" name="nombre2" id="txtnombre">
                <label for="txtid">Cedula :</label>
                <input class="form-control" type="text" name="cedula2" id="txtid">
                <label for="txttele">Teléfonos :</label>
                <input class="form-control" type="text" name="telefono2" id="txttele">
                <label for="txtcorreo">Correo :</label>
                <input class="form-control" type="text" name="correo2" id="txtcorreo">
              </div>
            </div>
          </div>

          <button class="btn btn-primary" onclick="return mostrarOcultar('ocultable')" type="button">Agregar Profesor Asesor</button><br><br>
          <div id="ocultable">
            <div class="form-group">
              <label for="othr_asesor">Asignación a otro Profesor :</label>
              <input type="text" class="form-control" name="othr_asesor" id="othr_asesor">
            </div>
          </div>

          <p>Artículo 40 del Reglamento aprobado por el Consejo Académico: La tesis sera preferiblemente obra
            <br>
            de un solo estudiante,<br>
            pero por razones especiales se permitira mas de un (1) estudiante ne una misma Tesis.
          </p>
          <div class="boton-enviar">
            <button type="submit" id="control2" name="subir" class="btn btn-primary btn-lg">Enviar</button>
          </div>

        </form>
      </div>




      <!--/.Content -->

      <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
      <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
      <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
      <!-----Control de radio, check y boton del formulario------>
      <script src="../js/control_form.js"></script>
    </body>

    </html>



<?php } else {
    header('Location: ../login/login.php');
  }
} else {
  header('Location: ../login/login.php');
} ?>