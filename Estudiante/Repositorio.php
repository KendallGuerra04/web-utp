<?php 
require ('../class/conexion.php');
session_start(); 
?>
<?php 
   if (isset($_SESSION["cedula"])) {
     if ($_SESSION["tem"] == "1") {
   	 	
 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../css/form_arch.css" media="screen" />
    <title>Estudiante</title>
  </head>
  <body>
<!------Barra de Navegación ------->
<!--Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../Estudiante/home.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Estudiante/home.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="../Estudiante/Trab_grad.php">Registro Trabajo de Graduación
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="../Estudiante/Repositorio.php">Repositorio
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                          <li class="nav-item ">
                            <a class="navbar-brand" href="../Estudiante/seguimiento.php">Seguimiento
                              <span class="sr-only">(current)</span>
                            </a>
                      </li>
            </ul>
              <ul class="navbar-nav navbar-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                      <img src="<?php  
                        echo $_SESSION["foto"]; 
                                ?>
                       " class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                                <?php  
                                      echo $_SESSION["nombre"]; 
                                  ?>
                  </a>
                      <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                        aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                      </div>
                </li>
              </ul>
          </div> 
    </div>
  </nav>
<!--/.Navbar -->
<!-- El tipo de codificación de datos, enctype, DEBE especificarse como sigue -->
  <div id="header">
  <?php 
                   if (isset($_SESSION["error2"])) {
                    if ($_SESSION["error2"] =="Success") {
                    ?>
                    <h6 class="alert alert-success text-center"><?php echo $_SESSION["mensaj"] ?></h6>
                    <?php
                    }elseif($_SESSION["error2"] =="error"){
                    ?>
                     <h6 class="alert alert-danger text-center"><?php echo $_SESSION["mensaj1"] ?></h6>
                    <?php
                    }
                }
          ?>
  </div>
  <div id="container">
        <div id="first">
        <form action="../controllers/up_files.php" enctype="multipart/form-data" method="POST">

            <h4>Repositorio</h4>
            <div class="form-group">
            <input type="file" class="form-control" id="control-file" name="archivo[]" multiple="" accept="application/pdf, .doc, .docx, .odf">
            </div>
            <div class="form-group">
            <button type="submit" name="cargar" class="btn btn-primary btn-sm">CARGAR ARCHIVO</button>
          </div>
          
          <table id="tablax" class="table table-striped table-bordered">
          <thead>
          <tr>
          <th scope="col">Nombre Archivo</th>
          <th scope="col">Tamaño</th>
          <th scope="col">Herrramienta</th>
          </tr>
          </thead>
          <tbody>
          <?php
          $cedula=$_SESSION['cedula'];
          $sql = "SELECT id_repos,ruta_arch,nombre_arch,size_arch from repos where cedula='$cedula'";
          $statement = Conexion::conectar()->prepare($sql);
          $statement->execute(); 
          $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $result) {
              ?>
          <tr>
          <td><a href="<?php echo $result['ruta_arch'];?>"><?php echo $result['nombre_arch'];?></a></td>
          <td>"<?php echo $result['size_arch'];?>"MB</td>
          <td><button type="submit" name="eliminar" id="prove" class="btn btn-secondary btn-sm">Eliminar</button></td>
          </tr>
          <?php
              } 
             ?>
        </tbody>
        </table>
        </form>
        </div>

            <div id="second">
            <form action="../controllers/up_file.php" enctype="multipart/form-data" method="POST">

            <h4>Anteproyecto</h4>
            <div class="form-group">
            <input type="file" class="form-control" id="control-file" name="archivo" accept="application/pdf, .doc, .docx, .odf" required>
            </div>
            <div class="form-group">
            <button type="submit" name="subir" class="btn btn-primary btn-sm">CARGAR ARCHIVO</button>
          </div>
          </form>
          <form action="../controllers/delete_file.php" method="POST">
          <table id="tablax1" class="table table-striped table-bordered">
          <thead>
          <tr>
          <th scope="col">Nombre Archivo</th>
          <th scope="col">Tamaño</th>
          <th scope="col">Herrramienta</th>
          </tr>
          </thead>
          <tbody>
          <?php
          $cedula=$_SESSION['cedula'];
          $sql = "SELECT ruta,nombre,tamanio from anteproyectos where cedula='$cedula'";
          $statement = Conexion::conectar()->prepare($sql);
          $statement->execute(); 
          $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach($results as $result) {
              ?>
          <tr>
          <td><a href="<?php echo $result['ruta'];?>"><?php echo $result['nombre'];?></a></td>
          <td><?php echo $result['tamanio'];?>MB</td>
          <td><button type="submit" name="eliminar" id="prove" class="btn btn-secondary btn-sm">Eliminar</button></td>
          </tr>
          <?php
              } 
             ?>
             </tbody>
        </table>
      </form>
        </div>

              <div id="clear">
              </div>
    
  </div>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="../js/table.js"></script>
  </body>
</html>
<?php }else{
         header('Location: ../login/login.php');

}
 }else{
         header('Location: ../login/login.php');

} ?>