<?php 
session_start(); 
if (isset($_SESSION["cedula"])) {
  if ($_SESSION["tem"] == "3") {
     
?> 
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Vicedecano</title>
  </head>
  <body>
<!------Barra de Navegación ------->
<!--Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../Estudiante/home.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Vicedecano_Academico/home.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="../Vicedecano_Academico/verif_ante.php">Verificación de Anteproyecto
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="../Vicedecano_Academico/listado.php">Anteproyectos Enviados
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                        <li class="nav-item ">
                          <a class="navbar-brand" href="../Vicedecano_Academico/reportes.php">Reportes
                            <span class="sr-only">(current)</span>
                          </a>
                        </li>
            </ul>
              <ul class="navbar-nav navbar-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                      <img src="<?php  
                        echo $_SESSION["foto"]; 
                                ?>
                       " class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                                <?php  
                                      echo $_SESSION["nombre"]; 
                                  ?>
                  </a>
                      <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                        aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                      </div>
                </li>
              </ul>
          </div> 
    </div>
  </nav>
<!--/.Navbar -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="../imagenes/slideshow_plantilla_simulacro850px.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="../imagenes/slidepatria.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="../imagenes/campdonasangre.jpg" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>

<?php   }else{
   header('Location: ../login/login.php');
}
 }else{
         header('Location: ../login/login.php');

} ?>