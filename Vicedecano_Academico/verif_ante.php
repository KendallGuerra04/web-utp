<?php 
session_start(); 
error_reporting(0);
require '../class/conexion.php';
?>
<?php 
if (isset($_SESSION["cedula"])) {
  if ($_SESSION["tem"] == "3") { 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/med_form.css" media="screen" />

    <title>Vicedecano</title>
    
    <style> 
      .margenes {
        margin:100px;
      }
    </style>
  </head>
  <body>
<!------Barra de Navegación ------->
<!--Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../Estudiante/home.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Vicedecano_Academico/home.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="../Vicedecano_Academico/verif_ante.php">Verificación de Anteproyecto
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="../Vicedecano_Academico/listado.php">Anteproyectos Enviados
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                        <li class="nav-item ">
                          <a class="navbar-brand" href="../Vicedecano_Academico/reportes.php">Reportes
                            <span class="sr-only">(current)</span>
                          </a>
                        </li>
            </ul>
              <ul class="navbar-nav navbar-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                      <img src="<?php  
                        echo $_SESSION["foto"]; 
                                ?>
                       " class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                                <?php  
                                      echo $_SESSION["nombre"]; 
                                  ?>
                  </a>
                      <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                        aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                      </div>
                </li>
              </ul>
          </div> 
    </div>
  </nav>
<!--/.Navbar -->
<div class="margenes">
<?php 
  if (isset($_SESSION["error"])) {
    echo $_SESSION["error"];
    if ($_SESSION["error"] == "Success") {
?>
  <h5 class="alert alert-success text-center"> <?php echo $_SESSION["mensaje"];?> </h5>
<?php
  }
 }
?>
<div class="border p-5 form border border-primary" >
        <h1>Listado</h1>
            <h5>Filtrar Por Sede y Carrera :</h5> <br>
    <form action="verif_ante.php" method="POST">
    <div class="form-row align-items-center">
        <label for="sede">Sede :</label>
            <select class="form-control" id="sede" name="sede" required>
              <option selected disabled value="">Seleccione la sede</option>
              <option value="Campus Metropolitano “Dr. Víctor Levi Sasso”">Campus Metropolitano “Dr. Víctor Levi Sasso”</option>
              <option value="Centro Regional de Veraguas" >Centro Regional de Veraguas</option>
              <option value="Centro Regional de Panamá Oeste" >Centro Regional de Panamá Oeste</option>
              <option value="Centro de Bocas del Toro" >Centro de Bocas del Toro</option>
              <option value="Centro Regional de Azuero" >Centro Regional de Azuero</option>
              <option value="Centro Regional de Chiriqui" >Centro Regional de Chiriqui</option>
              <option value="Centro Regional de Colón" >Centro Regional de Colón</option>
              <option value="Centro Regional de Coclé" >Centro Regional de Coclé</option>
                <option value="todos"> Todas... </option>
            </select>
    </div> <br>
    <div class="form-row align-items-center">
    <label for="carrera">Carrera :</label>
            <select class="form-control" id="carrera" name="carrera" required>
            <option selected disabled value="">Seleccione la Carrera</option>
                <option value="Licenciatura en Ciencias de la Computación">Licenciatura en Ciencias de la Computación</option>
                <option value="Licenciatura en Ingeniería de Sistemas de Información">Licenciatura en Ingeniería de Sistemas de Información</option>
                <option value="Licenciatura en Ingeniería de Sistemas y Computación">Licenciatura en Ingeniería de Sistemas y Computación</option>
                <option value="Licenciatura en Ingeniería de Software">Licenciatura en Ingeniería de Software</option>
                <option value="Licenciatura en Desarrollo de Software">Licenciatura en Desarrollo de Software</option>
                <option value="Licenciatura en Informática Aplicada a la Educación">Licenciatura en Informática Aplicada a la Educación</option>
                <option value="Licenciatura en Redes Informáticas">Licenciatura en Redes Informáticas</option>
                <option value="Técnico en Informática para la Gestión Empresarial">Técnico en Informática para la Gestión Empresarial</option>
                <option value="todos"> Todas... </option>
            </select>
    </div>
    <div style="text-align: right;"> <br> 
    <button type="submit" class="btn btn-primary btn-lg" name="filtro">Filtrar</button></div>
    <br>
    </form> <br>
    <div class="form-group">
            <input class="form-control mb-4" id="tableSearch" type="text" placeholder="Type something to search list items">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Cedula</th>
                <th>Anteproyecto</th>
            </tr>

            <?php
             $carrera = $_POST['carrera'];
             $sede = $_POST['sede'];
             if ($carrera != 'todos' && $sede == 'todos') {
               $sql = "SELECT cedula FROM proyectos WHERE carrera = '$carrera'";
             } 
             else {
               if ($sede != 'todos' && $carrera == 'todos') {
                 $sql = "SELECT cedula FROM proyectos WHERE sede = '$sede'";
               }
               else {
                 if ($carrera == 'todos' && $sede == 'todos') {
                   $sql = "SELECT cedula FROM proyectos";
                 }
                 else {
                  $sql = "SELECT cedula FROM proyectos WHERE sede = '$sede' AND carrera ='$carrera'";
                }
               }
             }
             $statement = Conexion::conectar()->prepare($sql);
             $statement->execute(); 
             $results = $statement->fetchAll(PDO::FETCH_ASSOC);
           
           foreach($results as $result ) {
            ?>
            </thead>
            <tbody id="myTable">
            <tr>
                <td> <?php echo $result['cedula']; ?> </td>
                <td><button class="btn btn-primary" onclick="location.href='../Vicedecano_Academico/form_ante.php?cedula=<?php echo $result['cedula']?>'" type="button" class="btn btn-secondary">Visualizar</button></td>
            </tr>
            </tbody>
            <?php } ?>
        </table>
  </div>
</div>
</div>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <script src="../js/control_vice.js"></script>
  </body>
</html>

<?php }else{
         header('Location: ../login/login.php');
}
 }else{
         header('Location: ../login/login.php');
} ?>