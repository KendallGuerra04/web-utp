<?php 
session_start(); 
require ('../class/conexion.php')
?>
<?php 
   if (isset($_SESSION["cedula"])) {
     if ($_SESSION["tem"] == "3") {
   	 	
 ?>
 

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/med_form.css" media="screen" />

    <title>Vicedecano</title>
  </head>
  <body>
<!------Barra de Navegación ------->
<!--Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../Estudiante/home.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Vicedecano_Academico/home.php">Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="../Vicedecano_Academico/verif_ante.php">Verificación de Anteproyecto
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="../Vicedecano_Academico/listado.php">Anteproyectos Enviados
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                        <li class="nav-item ">
                          <a class="navbar-brand" href="../Vicedecano_Academico/reportes.php">Reportes
                            <span class="sr-only">(current)</span>
                          </a>
                        </li>
            </ul>
              <ul class="navbar-nav navbar-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                      <img src="<?php  
                        echo $_SESSION["foto"]; 
                                ?>
                       " class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                                <?php  
                                      echo $_SESSION["nombre"]; 
                                  ?>
                  </a>
                      <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                        aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                      </div>
                </li>
              </ul>
          </div> 
    </div>
  </nav>
<!--/.Navbar -->
<form action="" class="border p-5 form border border-dark shadow-lg p-4 mb-4 bg-white" method="POST" enctype="multipart/form-data">
<div class="abs-center">
<div class="border p-5 form border border-primary" >
        <h3 class="text-center">Universidad Tecnológica de Panamá<br>
      Facultad De Ingenieria de Sistemas Computacionales<br>
    Reportes</h3>
            <h5>Filtrar Por Sede y Carrera :</h5>
    <div class="form-row align-items-center">
        <label for="sede">Sede :</label>
            <select class="form-control" name="s_sede">
                <option value="Seleccione la sede" selected>Seleccione la sede</option>
                <option value="Campus Metropolitano “Dr. Víctor Levi Sasso”">Campus Metropolitano “Dr. Víctor Levi Sasso”</option>
              <option value="Centro Regional de Veraguas" >Centro Regional de Veraguas</option>
              <option value="Centro Regional de Panamá Oeste" >Centro Regional de Panamá Oeste</option>
              <option value="Centro de Bocas del Toro" >Centro de Bocas del Toro</option>
              <option value="Centro Regional de Azuero" >Centro Regional de Azuero</option>
              <option value="Centro Regional de Chiriqui" >Centro Regional de Chiriqui</option>
              <option value="Centro Regional de Colón" >Centro Regional de Colón</option>
              <option value="Centro Regional de Coclé" >Centro Regional de Coclé</option>
            </select>
    </div>
    <div class="form-row align-items-center">
    <label for="carrera">Carrera :</label>
            <select class="form-control"  name="s_carreras" > 
                <option  value="Seleccione la Carrera" selected>Seleccione la Carrera</option>
                <option value="Licenciatura en Ciencias de la Computación" >Licenciatura en Ciencias de la Computación </option>
                <option value="Licenciatura en Ingeniería de Sistemas de Información" >Licenciatura en Ingeniería de Sistemas de Información</option>
                <option value="Licenciatura en Ingeniería de Sistemas y Computación" >Licenciatura en Ingeniería de Sistemas y Computación</option>
                <option value="Licenciatura en Ingeniería de Software" > Licenciatura en Ingeniería de Software</option>
                <option value="Licenciatura en Desarrollo de Software" >Licenciatura en Desarrollo de Software</option>
                <option value="Licenciatura en Informática Aplicada a la Educación" >Licenciatura en Informática Aplicada a la Educación</option>
                <option value="Licenciatura en Redes Informáticas" >Licenciatura en Redes Informáticas</option>
                <option value="Técnico en Informática para la Gestión Empresarial">Técnico en Informática para la Gestión Empresarial</option>
            </select>
    </div><br>
    <div style="text-align: right;">
    <button type="submit" class="btn btn-primary btn-lg" name="filtro">Filtrar</button></div>
    <br>
    </form>​​
    <?php 
    $sede_sel= NULL;
    $carrera_sel= NULL;

    if(isset($_REQUEST['s_sede']) && $_REQUEST["s_sede"]!= NULL && isset($_REQUEST['s_carreras']) && $_REQUEST["s_carreras"]!= NULL){
      $sede_sel=$_REQUEST["s_sede"];
      $carrera_sel=$_REQUEST['s_carreras'];
    }
    ?>
    
    <div class="form-group">
        <table class="table table-bordered table-striped text-center">
            <thead>
            <tr>
                <th>Modalidad de trabajo de<br>Graduación</th>
                <th>Cantidad de <br>Estudiantes</th>
            </tr>
            </thead>
            <tbody id="myTable" class="text-left">
            <tr>
            <td>Teórico
            <?php $conn = Conexion::conectar();
                        $tipo_p="Teórico";
                        $sql = "SELECT count(1) from proyectos where tipo = '$tipo_p' and sede='$sede_sel' and carrera='$carrera_sel'";
                        $result=$conn->query($sql);
                        $total=$result->fetchColumn();
                 ?></td>
                <td>
                <?php
                echo $total;
                ?>
                </td>
            </tr>
            <tr>
                <td>Teórico Práctico<?php $conn = Conexion::conectar();
                        $tipo_p="Teórico Práctico";
                        $sql = "SELECT count(1) from proyectos where tipo = '$tipo_p' and sede='$sede_sel' and carrera='$carrera_sel'";
                                                $result=$conn->query($sql);
                        $total=$result->fetchColumn();
                 ?></td>
                <td>
                <?php
                echo $total;
                ?>
                </td>
            </tr>
            <tr>
                <td>Práctica Profesional
                <?php $conn = Conexion::conectar();
                        $tipo_p="Práctica Profesional";
                        $sql = "SELECT count(1) from proyectos where tipo = '$tipo_p' and sede='$sede_sel' and carrera='$carrera_sel'";
                        $result=$conn->query($sql);
                        $total=$result->fetchColumn();
                 ?></td>
                <td>
                <?php
                echo $total;
                ?>
                </td>
            </tr>
            <tr>
                <td>Certificación
                <?php $conn = Conexion::conectar();
                        $tipo_p="Certificación";
                        $sql = "SELECT count(1) from proyectos where tipo = '$tipo_p' and sede='$sede_sel' and carrera='$carrera_sel'";                        $result=$conn->query($sql);
                        $total=$result->fetchColumn();
                 ?></td>
                <td>
                <?php
                echo $total;
                ?>
                </td>
            </tr>
            <tr>
                <td>Otro
                <?php $conn = Conexion::conectar();
                        $tipo_p="Otro";
                        $sql = "SELECT count(1) from proyectos where tipo = '$tipo_p' and sede='$sede_sel' and carrera='$carrera_sel'";                        $result=$conn->query($sql);
                        $total=$result->fetchColumn();
                 ?></td>
                <td>
                <?php
                echo $total;
                ?>
                </td>
            </tr>
            </tbody>
        </table>
  </div>
</div>
</div>
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <script src="../js/control_vice.js"></script>
  </body>
</html>

<?php }else{
         header('Location: ../login/login.php');

}
 }else{
         header('Location: ../login/login.php');

} ?>