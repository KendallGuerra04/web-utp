
<?php 
session_start(); 
?>
<?php 
   if (isset($_SESSION["cedula"])) {
     if ($_SESSION["tem"] == "3") {
       $cedula = $_REQUEST['cedula'];
 ?>
 <!doctype html>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/med_form.css" media="screen" />

    <title>Vicedecano</title>
  </head>
  <body style="background-image: url(../imagenes/WebBackground.jpg); background-repeat: repeat; background-size: contain">
<!------Barra de Navegación ------->
<!--Navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="../Estudiante/home.php">
        <img src="../imagenes/logo_utp_1_72.jpg" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        GRADUACIÓN FISC
      </a>
        <button class="navbar-toggler" data-target="#menu" data-toggle="collapse" type="button" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
          <div class="collapse navbar-collapse" id="menu">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item active">
                <a class="navbar-brand" href="../Vicedecano_Academico/home.php"> Inicio
                  <span class="sr-only">(current)</span>
                </a>
              </li>
                  <li class="nav-item ">
                    <a class="navbar-brand" href="../Vicedecano_Academico/verif_ante.php"> Verificación de Anteproyecto
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                      <li class="nav-item ">
                        <a class="navbar-brand" href="../Vicedecano_Academico/listado.php"> Anteproyectos Enviados
                          <span class="sr-only">(current)</span>
                        </a>
                      </li>
                        <li class="nav-item ">
                          <a class="navbar-brand" href="../Vicedecano_Academico/reportes.php"> Reportes
                            <span class="sr-only">(current)</span>
                          </a>
                        </li>
            </ul>
              <ul class="navbar-nav navbar-right nav-flex-icons">
                <li class="nav-item avatar dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                      <img src="<?php  
                        echo $_SESSION["foto"]; 
                                ?>
                       " class="rounded-circle z-depth-0"
                        alt="avatar image" height="35">
                                <?php  
                                      echo $_SESSION["nombre"]; 
                                  ?>
                  </a>
                      <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
                        aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="../login/logout.php">Cerrar Sesión</a>
                      </div>
                </li>
              </ul>
          </div> 
    </div>
  </nav>
<!--/.Navbar -->
<div class="abs-center">
    <form method="POST" action="../controllers/llenar_form_ante.php?cedula=<?php echo $cedula; ?>" id="prueba" class="border p-5 border-dark shadow-lg p-4 mb-4 bg-white">
        <h4 class="text-center">Universidad Tecnológica de Panamá <br>
        Facultad de Ingenieria de Sistemas Computacionales <br>
        Vicedecanato Académico<br>
        Formulario de Verificación de Entrega de Documentos
        </h4>
        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro" name="apro" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p id="down1">1. Página de Presentación Usando el Formato respectivo.<br>
                *Nombre de la ​Universidad, Nombre de la Facultad, Título del<br>
                Trabajo, Tipo de Trabajo, Nombre del Profesor Asesor, Integrantes(s),<br>
                Titulo a Optar, Año Elaboración.</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro2" name="apro2" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p id="down2">2. Formulario de Registro Oficial del tema del trabajo de graduación<br>
             firmado por el (los) estudiantes(s) y el profesor Asesor Propuesto.</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro3" name="apro3" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p id="down3">3. Introducción
                Incluye descripción de la situación actual, propuesto y las mejoras que<br>
                el proyecto persigue. Debe incluir definición y alcance del tema,<br>
                metodología y técnica de investigación a utilizar.</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro4" name="apro4" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p>4. Índice del Anteproyecto</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro5" name="apro5" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p>5. Objetivos (General y Especificos)</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro6" name="apro6" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p>6. Plan de Contenido propuesto para el desarrollo del proyecto.</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro7" name="apro7" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p id="down4">7. Bibliografía
                Generalmente el minimo son 10 referencias bibliográficas. Lo <br>
                importante es que sea actualizado de acuerdo al tema(por lo menos<br> 
                los ultimos 5 años).</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro8" name="apro8" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p id="down5">8. Cronograma de actividades
                Presentado en diagrama de Gantt, <br>
                preferiblemente en términos de meses o semanas.<br>  
                Los meses o semanas no deben ser etiquetados cronológicamente.<br> 
                El formato a utilizar es: Mes 1, Mes 2: Semana1; Semana 2.</p>
        </div>

        <div class="form-row align-items-center">
          <select class="form-control col-sm-3 my-1 text-center" id="apro9" name="apro9" required>
                  <option selected disabled value="">Seleccionar</option>
                  <option>Aprobado</option>
                  <option>Rechazado</option>
                  <option>Modificable</option>
              </select>
            <p>9. Herramientas de Software y Hardware a utilizar.</p>
        </div>
        <div id="control" class="form-row align-items-center">
        <button type="button" onclick="location.href='../Vicedecano_Academico/verif_ante.php'" class="btn btn-dark">Volver</button> <p> &nbsp; </p>
        <button type="submit" class="btn btn-dark">Revisado</button>
        </div>
  </form>
</div>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>

<?php }else{
         header('Location: ../login/login.php');
}
 }else{
         header('Location: ../login/login.php');
} ?>